import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import {AngularFire} from 'angularfire2';

@Injectable()
export class InvoicesService {

  invoicesObservable;

  getInvoices(){
    return this.invoicesObservable = this.af.database.list('/invoices').delay(2000);
  }

  addInvoice(invoice){
    this.invoicesObservable.push(invoice);
  }

  deleteInvoice(invoice){
    this.af.database.object('/invoices/'+invoice.$key).remove();
  }

  constructor(private af:AngularFire) { }

}
