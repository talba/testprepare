import { Component, OnInit } from '@angular/core';
import { InvoicesService } from './invoices.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {

  invoices;
  isLoading:Boolean=true;
  currentInvoice;

  changeCurrentInvoice(invoice){
    this.currentInvoice=invoice;
  }

  addInvoice(invoice){
    this._invoicesService.addInvoice(invoice);
  }

  deleteInvoice(invoice){
    this._invoicesService.deleteInvoice(invoice);
  }

  constructor(private _invoicesService:InvoicesService) { }

  ngOnInit() {
     this._invoicesService.getInvoices().subscribe(invoicesData => {
       this.invoices=invoicesData;
       this.isLoading=false;
     })
  }

}
