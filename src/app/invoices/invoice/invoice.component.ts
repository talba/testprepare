import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Invoice } from './invoice';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
  inputs: ['invoice'],
})
export class InvoiceComponent implements OnInit {

  invoice:Invoice;
  isEdit:Boolean=false;
  editButtonText:String="Edit";
  oldInvoice:Invoice;

  @Output() deleteInvoiceEvent = new EventEmitter <Invoice>();
  @Output() updateInvoiceEvent = new EventEmitter <Invoice>();

  startDelete(){
     this.deleteInvoiceEvent.emit(this.invoice);
   }
   toggleEdit(){
     this.isEdit=!this.isEdit
     this.isEdit ? this.editButtonText="Save" : this.editButtonText="Edit";
     if(this.isEdit){
       this.oldInvoice=Object.assign({},this.invoice);
     }
      if(!this.isEdit){
        this.updateInvoiceEvent.emit(this.invoice);
     }
   }
   cancelEdit(){
     this.invoice=Object.assign({},this.oldInvoice);
     this.toggleEdit();
   }

  constructor() { }

  ngOnInit() {
  }

}
