import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import {AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceFormComponent } from './invoices/invoice-form/invoice-form.component';
import { InvoiceComponent } from './invoices/invoice/invoice.component';
import {InvoicesService} from './invoices/invoices.service';

export const firebaseConfig = {
    apiKey: "AIzaSyBQykNVfqUqKn3VRss83CcnLw6ITbrpN9I",
    authDomain: "testprepare-df7c8.firebaseapp.com",
    databaseURL: "https://testprepare-df7c8.firebaseio.com",
    storageBucket: "testprepare-df7c8.appspot.com",
    messagingSenderId: "897182047934"
  }

const appRoutes : Routes = [
  {path: 'invoices', component: InvoicesComponent},
  {path: 'invoice-form', component: InvoiceFormComponent},
  {path: '', component: InvoiceFormComponent},
  {path: '**', component: PageNotFoundComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    InvoiceFormComponent,
    InvoicesComponent,
    InvoiceFormComponent,
    InvoiceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  
  providers: [InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
