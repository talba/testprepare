import { TextPreparePage } from './app.po';

describe('text-prepare App', function() {
  let page: TextPreparePage;

  beforeEach(() => {
    page = new TextPreparePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
